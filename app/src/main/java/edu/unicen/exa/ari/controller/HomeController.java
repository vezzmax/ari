package edu.unicen.exa.ari.controller;

import java.io.IOException;

import org.apache.lucene.queryparser.classic.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import edu.unicen.exa.ari.model.SearchDTO;
import edu.unicen.exa.ari.model.dto.SearchResultDTO;
import edu.unicen.exa.ari.service.LegalSearchService;

@Controller
public class HomeController {

    private static final Logger LOG = LoggerFactory.getLogger(SearchController.class);

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView index(Model model) {
        model.addAttribute("command", new SearchDTO());
        return new ModelAndView("index");
    }

    @Autowired
    LegalSearchService legalSearchService;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String processSubmit(@ModelAttribute("command") SearchDTO searchDTO,
                                BindingResult result) {
        //ModelAndView modelResult = new ModelAndView("searchResults");
        try {
            SearchResultDTO searchResult = legalSearchService.search(searchDTO.getQuery());
            //modelResult.addObject("result", searchResult);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //return modelResult;
        return "finish.jsp";
    }
}
