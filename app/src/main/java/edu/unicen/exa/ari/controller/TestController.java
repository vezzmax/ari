package edu.unicen.exa.ari.controller;

import java.io.IOException;

import org.apache.lucene.queryparser.classic.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import edu.unicen.exa.ari.model.SearchDTO;
import edu.unicen.exa.ari.model.dto.SearchResultDTO;
import edu.unicen.exa.ari.service.LegalSearchService;

@Controller
public class TestController {

    private static final Logger LOG = LoggerFactory.getLogger(TestController.class);

    @Autowired
    LegalSearchService legalSearchService;

    @RequestMapping(value = "/begin", method = RequestMethod.GET)
    public String displayLogin(Model model) {
        model.addAttribute("command", new SearchDTO());
        return "test.jsp";
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public ModelAndView search(@ModelAttribute("command") SearchDTO searchDTO,
                                      Model model) {

        LOG.info("Query: {}", searchDTO.getQuery());
        ModelAndView modelResult = new ModelAndView("searchResults.jsp");
        try {
            SearchResultDTO searchResult = legalSearchService.search(searchDTO.getQuery());
            modelResult.addObject("result", searchResult);
            modelResult.addObject("cases", searchResult.getResults());
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return modelResult;
    }
}
