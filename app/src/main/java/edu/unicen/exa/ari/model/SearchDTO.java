package edu.unicen.exa.ari.model;

/**
 * Created by mvezzosi on 6/2/15.
 */
public class SearchDTO {

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    private String query;

    @Override
    public String toString() {
        return "SearchDTO{" +
                "query='" + query + '\'' +
                '}';
    }
}
