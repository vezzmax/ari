function search(query) {
    var searchType = $('#searchType').val();
    if (searchType == '1') {
        return query;
    } else {
        return searchType + ":" + query;
    }
}

$("#search").submit(function (event) {
    var originalValue = $("#query").val();
    var changedQuery = search(originalValue);
    $("#query").val(changedQuery);
    this.submit();
});

$("#submitSpan").click(function (event) {
    $(this).closest('form').submit();
});
