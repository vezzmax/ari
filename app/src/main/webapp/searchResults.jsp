<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Resultados de la busqueda</title>
    <style type="text/css"></style>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel='stylesheet' href='webjars/bootstrap/3.3.4/css/bootstrap.min.css'>
</head>
<body>
<div class="container">
    <h2>Resultados encontrados: ${result.hits}</h2>
    <table class="table table-hover">
        <c:forEach var="caso" items="${cases}">
            <tr>
                <td><a href="${caso.link}">${caso.caseName}</a></td>
            </tr>
        </c:forEach>
    </table>

    </div>
</body>
</html>
