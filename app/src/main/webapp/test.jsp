<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Busqueda de Textos Legales - Análisis y Recuperacion de la Información 2015</title>
    <style type="text/css"></style>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel='stylesheet' href='webjars/bootstrap/3.3.4/css/bootstrap.min.css'>
    <link rel='stylesheet' href='css/app.css'>
</head>
<body>

<div class="navbar navbar-custom navbar-fixed-top">
    <div class="navbar-header"><a class="navbar-brand" href="#">Textos Legales</a>
        <a class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
    </div>
    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <li class="active"><a href="http://www.exa.unicen.edu.ar/catedras/ayrdatos/">Home</a></li>
            <li><a href="http://www.exa.unicen.edu.ar/catedras/ayrdatos/">Catedras</a></li>
            <li>&nbsp;</li>
        </ul>
        <form:form id="search" action="search" class="navbar-form">
            <div class="form-group" style="display:inline;">
                <div class="input-group">
                    <div class="input-group-btn">
                        <select id="searchType" class="form-control">
                            <option value="1">By Keyword</option>
                            <option value="name">By Name</option>
                            <option value="catchphrase">By Catchphrase</option>
                            <option value="sentence">By Sentence</option>
                        </select>
                    </div>
                    <form:input id="query" path="query" cssClass="form-control" placeholder="What are searching for?"
                                style="width:500px"/>
                    <span id="submitSpan" class="input-group-addon">
                        <span type="submit" class="glyphicon glyphicon-search"></span>
                    </span>
                </div>
            </div>
        </form:form>
    </div>
</div>
<script type="text/javascript" src="webjars/jquery/2.1.4/jquery.js"></script>
<script type="text/javascript" src="webjars/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/search.js"></script>
</body>
</html>
