package edu.unicen.exa.ari.config;

import java.io.File;
import java.io.IOException;

import org.apache.camel.spring.javaconfig.CamelConfiguration;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@ComponentScan(basePackages = { "edu.unicen.exa.ari.route", "edu.unicen.exa.ari.service" })
@PropertySource("classpath:ari.properties")
public class LuceneConfig extends CamelConfiguration {

    @Value("${index.destination.folder}")
    protected String indexFolderDestination;

    private final Version version = Version.LUCENE_44;

    @Bean
    public StandardAnalyzer luceneAnalyzer() {
        return new StandardAnalyzer(version);
    }

    @Bean
    public FSDirectory luceneDirectory() throws IOException {
        return SimpleFSDirectory.open(new File(indexFolderDestination));
    }

    @Bean
    public IndexWriterConfig indexWriterConfig() {
        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(version, luceneAnalyzer());
        indexWriterConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        return indexWriterConfig;
    }

    @Bean
    public QueryParser queryParser() {
        return new QueryParser(version, "all", luceneAnalyzer());
    }

    @Bean
    public IndexWriter indexWriter() throws IOException {
        return new IndexWriter(luceneDirectory(), indexWriterConfig());
    }

    @Bean
    public IndexSearcher indexSearcher() throws IOException {
        IndexReader indexReader = DirectoryReader.open(luceneDirectory());
        IndexSearcher indexSearcher = new IndexSearcher(indexReader);
        return indexSearcher;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}
