package edu.unicen.exa.ari.model.dto;

public class CaseDTO {
    private String caseName;
    private String link;

    public String getCaseName() {
        return caseName;
    }

    public void setCaseName(String caseName) {
        this.caseName = caseName;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return "Case{" +
                "caseName='" + caseName + '\'' +
                ", link='" + link + '\'' +
                '}';
    }
}
