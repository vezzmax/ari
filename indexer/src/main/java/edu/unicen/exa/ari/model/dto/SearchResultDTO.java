package edu.unicen.exa.ari.model.dto;

import java.util.ArrayList;
import java.util.List;

public class SearchResultDTO {

    private int hits;

    private List<CaseDTO> results = new ArrayList<CaseDTO>();

    public int getHits() {
        return hits;
    }

    public void setHits(int hits) {
        this.hits = hits;
    }

    public List<CaseDTO> getResults() {
        return results;
    }

    public void setResults(List<CaseDTO> results) {
        this.results = results;
    }
}
