package edu.unicen.exa.ari.route;

import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.apache.camel.spi.DataFormat;
import org.apache.camel.spring.SpringRouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import edu.unicen.exa.ari.model.ObjectFactory;

@Component
public class IndexRoute extends SpringRouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(IndexRoute.class);
    public static final String ROUTE_ID = "INDEX_ROUTE";
    public static final String QUEUE_ROUTE_ID = "QUEUE_ROUTE";

    @Value("${data.source.folder}")
    private String folderToIndex;

    private DataFormat dataFormat;

    public IndexRoute() {
        LOG.info("Folder to Index: {}", folderToIndex);
        dataFormat = new JaxbDataFormat(ObjectFactory.class.getPackage().getName());
    }

    @Override
    public void configure() throws Exception {

        //onCompletion().to("bean:indexWriter?method=close");

        from("file:" + folderToIndex + "?noop=true").routeId(ROUTE_ID).to("seda:case").end();

        from("seda:case").routeId(QUEUE_ROUTE_ID).unmarshal(dataFormat).to("bean:indexer?method=index").end();

    }

}
