package edu.unicen.exa.ari.service;

import java.io.IOException;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.unicen.exa.ari.model.CaseType;
import edu.unicen.exa.ari.model.CatchphraseType;
import edu.unicen.exa.ari.model.SentenceType;

@Component("indexer")
public class DefaultLegalIndexerService implements LegalIndexerService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultLegalIndexerService.class);
    public static final String SEPARATOR = " ";
    public static final String NAME = "name";
    public static final String LINK = "link";
    public static final String CATCHPHRASE = "catchphrase";
    public static final String SENTENCE = "sentence";
    public static final String ALL = "all";

    @Autowired
    IndexWriter indexWriter;

    private StringBuffer all;
    private Document document;

    @Override
    public void index(final CaseType aCase) throws IOException {
        LOG.info("Indexing case name: {}", aCase.getName());
        all = new StringBuffer();
        document = new Document();

        createNameField(aCase);
        createLinkField(aCase);
        createCatchPhraseField(aCase);
        createSentenceField(aCase);
        createAllField();

        indexWriter.addDocument(document);
        // indexWriter.close();
    }

    private void createAllField() {
        TextField allField = new TextField(ALL, all.toString(), Field.Store.NO);
        document.add(allField);
    }

    private void createSentenceField(CaseType aCase) {
        TextField sentenceField;
        for (SentenceType sentence : aCase.getSentences().getSentence()) {
            sentenceField = new TextField(SENTENCE, sentence.getValue(), Field.Store.NO);
            sentenceField.setBoost(2.0f);
            all.append(sentence.getValue() + SEPARATOR);
            document.add(sentenceField);
        }
    }

    private void createCatchPhraseField(CaseType aCase) {
        TextField catchPhraseField;
        for (CatchphraseType catchphrase : aCase.getCatchphrases().getCatchphrase()) {
            catchPhraseField = new TextField(CATCHPHRASE, catchphrase.getValue(), Field.Store.NO);
            catchPhraseField.setBoost(3.0f);
            all.append(catchphrase.getValue() + SEPARATOR);
            document.add(catchPhraseField);
        }
    }

    private void createLinkField(CaseType aCase) {
        StringField linkField = new StringField(LINK, aCase.getAustLII(), Field.Store.YES);
        document.add(linkField);
    }

    private void createNameField(CaseType aCase) {
        TextField nameField = new TextField(NAME, aCase.getName(), Field.Store.YES);
        nameField.setBoost(4.0f);
        all.append(aCase.getName() + SEPARATOR);
        document.add(nameField);
    }

}
