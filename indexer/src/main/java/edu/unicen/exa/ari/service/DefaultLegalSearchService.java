package edu.unicen.exa.ari.service;

import java.io.IOException;

import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.Explanation;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import edu.unicen.exa.ari.model.dto.CaseDTO;
import edu.unicen.exa.ari.model.dto.SearchResultDTO;

@Component
public class DefaultLegalSearchService implements LegalSearchService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultLegalSearchService.class);

    @Autowired
    private IndexSearcher indexSearcher;

    @Autowired
    private QueryParser queryParser;

    @Value(value = "${search.maxresults}")
    private Integer maxResults;

    @Override
    public SearchResultDTO search(String searchQuery) throws ParseException, IOException {
        Query queryToSearch = queryParser.parse(searchQuery);
        LOG.debug("Query to search: {}", queryToSearch);
        TopScoreDocCollector collector = TopScoreDocCollector.create(maxResults, true);
        indexSearcher.search(queryToSearch, collector);
        SearchResultDTO searchResultDTO = new SearchResultDTO();

        ScoreDoc[] hitsTop = collector.topDocs().scoreDocs;
        LOG.info("Search produced {} hits.", hitsTop.length);
        searchResultDTO.setHits(hitsTop.length);
        for (int i = 0; i < hitsTop.length; ++i) {
            int docId = hitsTop[i].doc;
            Document docAtHand = indexSearcher.doc(docId);
            LOG.debug("name: {}     link:{}", docAtHand.get("name"),docAtHand.get("link"));
            Explanation explanation = indexSearcher.explain(queryToSearch,
                    hitsTop[i].doc);
            LOG.debug("Explanation: {}", explanation);

            CaseDTO caseDTO = new CaseDTO();
            caseDTO.setCaseName(docAtHand.get("name"));
            caseDTO.setLink(docAtHand.get("link"));
            searchResultDTO.getResults().add(caseDTO);
        }

        return searchResultDTO;
    }
}
