package edu.unicen.exa.ari.service;

import java.io.IOException;

import edu.unicen.exa.ari.model.CaseType;

public interface LegalIndexerService {

    void index(CaseType aCase) throws IOException;
}
