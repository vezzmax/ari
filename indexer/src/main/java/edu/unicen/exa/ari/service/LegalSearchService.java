package edu.unicen.exa.ari.service;

import java.io.IOException;

import org.apache.lucene.queryparser.classic.ParseException;

import edu.unicen.exa.ari.model.dto.SearchResultDTO;

public interface LegalSearchService {

    SearchResultDTO search(String searchQuery) throws ParseException, IOException;
}
