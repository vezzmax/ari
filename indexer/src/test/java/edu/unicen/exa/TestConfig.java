package edu.unicen.exa;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.SimpleFSDirectory;
import org.springframework.context.annotation.Bean;

import edu.unicen.exa.ari.config.LuceneConfig;

public class TestConfig extends LuceneConfig {

    @Bean
    public IndexSearcher indexSearcher() throws IOException {
        IndexReader indexReader =
                DirectoryReader
                        .open(SimpleFSDirectory.open(new File("src/test/resources/search-index/")));
        IndexSearcher indexSearcher = new IndexSearcher(indexReader);
        return indexSearcher;
    }
}
