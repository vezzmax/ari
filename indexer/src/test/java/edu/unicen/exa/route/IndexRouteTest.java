package edu.unicen.exa.route;

import java.util.concurrent.TimeUnit;

import org.apache.camel.EndpointInject;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.NotifyBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;

import edu.unicen.exa.AriTests;
import edu.unicen.exa.ari.route.IndexRoute;

public class IndexRouteTest extends AriTests {

    @Value("${data.source.folder}")
    private String folderToIndex;

    @EndpointInject(uri = "mock:direct:end")
    protected MockEndpoint endEndpoint;

    @EndpointInject(uri = "mock:direct:error")
    protected MockEndpoint errorEndpoint;

    @Produce(uri = "direct:start")
    protected ProducerTemplate testProducer;

    @Test
    public void testRoute() throws Exception {
        endEndpoint.expectedMessageCount(1);
        errorEndpoint.expectedMessageCount(0);
        //testProducer.stop();
        //endEndpoint.assertIsSatisfied();
        //errorEndpoint.assertIsSatisfied();
        NotifyBuilder notify = new NotifyBuilder(context);
        notify.fromRoute(IndexRoute.ROUTE_ID).whenDone(3000).create();
        testProducer.start();
        boolean isCompleted = notify.matches(3, TimeUnit.SECONDS);
        //assertTrue(isCompleted);
    }
}
