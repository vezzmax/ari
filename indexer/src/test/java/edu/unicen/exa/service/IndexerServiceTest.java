package edu.unicen.exa.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.lucene.index.IndexWriter;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.xml.XMLParser;
import org.apache.tika.sax.BodyContentHandler;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.xml.sax.SAXException;

import edu.unicen.exa.AriTests;
import edu.unicen.exa.ari.model.CaseType;
import edu.unicen.exa.ari.service.LegalIndexerService;

public class IndexerServiceTest extends AriTests {

    private static final Logger LOG = LoggerFactory.getLogger(IndexerServiceTest.class);

    @Autowired
    IndexWriter indexWriter;

    @Autowired
    LegalIndexerService indexerService;

    @Test
    public void testTika() throws TikaException, SAXException, IOException {
        // detecting the file type
        BodyContentHandler handler = new BodyContentHandler();
        Metadata metadata = new Metadata();
        InputStream inputstream = getFile("06_1001.xml");
        ParseContext pcontext = new ParseContext();

        // Xml parser
        XMLParser xmlparser = new XMLParser();
        xmlparser.parse(inputstream, handler, metadata, pcontext);
        LOG.info("Contents of the document: {}", handler.toString());
    }

    private InputStream getFile(final String fileName) {
        return getClass().getClassLoader().getResourceAsStream(fileName);
    }

    @Test
    public void testParser() throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(CaseType.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        InputStream inputstream = getFile("caseSample.xml");

        CaseType caseType = (CaseType) unmarshaller.unmarshal(inputstream);

        assertNotNull(caseType.getName());
        assertEquals("SZAIX v Minister for Immigration"
                + " & Multicultural & Indigenous Affairs [2006] FCA 3 (10 January 2006)", caseType.getName());
        assertNotNull(caseType.getAustLII());
        assertEquals("http://www.austlii.edu.au/au/cases/cth/FCA/2006/3.html", caseType.getAustLII());
        assertNotNull(caseType.getCatchphrases());
        assertNotNull(caseType.getSentences());
    }

    @Test
    public void testIndex() throws JAXBException, IOException {
        JAXBContext jc = JAXBContext.newInstance(CaseType.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        InputStream inputstream = getFile("caseSample.xml");

        CaseType caseType = (CaseType) unmarshaller.unmarshal(inputstream);

        indexerService.index(caseType);
    }
}
