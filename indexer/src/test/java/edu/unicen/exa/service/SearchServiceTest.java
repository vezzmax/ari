package edu.unicen.exa.service;

import java.io.IOException;

import org.apache.lucene.queryparser.classic.ParseException;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import edu.unicen.exa.AriTests;
import edu.unicen.exa.ari.service.LegalSearchService;

public class SearchServiceTest extends AriTests {

    private static final Logger LOG = LoggerFactory.getLogger(SearchServiceTest.class);

    @Autowired
    LegalSearchService searchService;

    @Test
    public void testSearch() throws IOException, ParseException {
        searchService.search("Barbara");
    }
}
